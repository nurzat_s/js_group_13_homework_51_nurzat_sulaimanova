import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent  {
  @Input() name = '#1';
  @Input() icon = 'https://proprikol.ru/wp-content/uploads/2020/12/salyut-krasivye-kartinki-32.jpg';
}
