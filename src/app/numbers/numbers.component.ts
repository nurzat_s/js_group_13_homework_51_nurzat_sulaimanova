import { Component } from '@angular/core';

@Component({
  selector: 'app-numbers',
  templateUrl: './numbers.component.html',
  styleUrls: ['./numbers.component.css']
})
export class NumbersComponent {
  numbers: number[] = [];
  constructor() {
    this.numbers = this.generateNumbers();
  }

  generateNumbers() {
    const randomNumber: number[] = [];
    for (let i = 1; i <= 5; i++) {
      const random = Math.floor(Math.random() * (36 - 5 + 1)) + 5;
      if(randomNumber.includes(random)) {
        i--;
      } else {
        randomNumber.push(random);
      }
      randomNumber.sort(function (a,b) {
        return a - b;
      });
    }
    return randomNumber;
  }

  generateNewNumbers() {
    this.numbers = this.generateNumbers();
  }
}



