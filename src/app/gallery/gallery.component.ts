import { Component } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent {
  image = '';
  name = '';
  password = '';
  showForm = false;

  pictures = [
    {name: '#1',  image: 'https://proprikol.ru/wp-content/uploads/2020/12/salyut-krasivye-kartinki-32.jpg'},
    {name: '#2',  image: 'https://attuale.ru/wp-content/uploads/2019/01/1kprkerke.jpg'},
    {name: '#3',  image: 'https://hsto.org/getpro/habr/post_images/acd/97d/b7e/acd97db7e41f9b951f503412f53f92e4.jpg'},
  ]

  createImage() {
    this.pictures.push({
      image: this.image,
      name: this.name,
    })
  }

  setPassword(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.password = target.value;
  }

  checkPassword(){
    if (this.password === 'asdf') {
      this.showForm = !this.showForm;
    } else {
      alert('Enter correct password: asdf');
    }
  }




}
